#!/bin/sh

VERSION='V3.0.0'

RARS_URL="https://github.com/TheThirdOne/rars/releases/download/v1.5/rars1_5.jar"
RARS='rars1_5.jar'
EXTENSION1='.s'
EXTENSION2='.asm'
OFFSET='4'
SEGMENT='.text'
BYTE2BYTE='false'
INCLUDESIZE='false'
SAVEOUTPUT='false'
MEMCONFIG='Default'
TEXTFIRSTADRESS='0x00400000'
DATAFIRSTADRESS='0x10010000'
SOURCEFILES=''
VERBOSE='false'

usage(){
	echo "usage: $0 -[OPTION...] [TextAtZero|DataAtZero] FILE.s|FILE.asm"
	echo "See README for further info about the options available."
	exit 0
}

err(){
	echo "error: $1"
	exit $2
}

verbErr(){
	echo " Uh-oh... Fatal/Calamitous/Cataclysmic/Catastrophic/Destructive/Disastrous/Fateful/Mortal/Ruinous error! What is going on? Lord take the wheel...\n\n Oh, nevermind. I think that I figured it out:\n"
}

explanation(){
	echo -n "\nSeems like you want to match machine code with an address... That is boring as f@#% innit? Let me see... \"$BASENAME$FILEEXT\"? Not a big deal. Maybe RARS could dump the $SEGMENT segment for me to make things easy. I could tell him to dump it in \"$HEXFILE\"... Yep, sounds cool. Keep in mind that according to your instructions, the first address will be "
	case "$SEGMENT" in
		'.text')
		echo "$TEXTFIRSTADRESS.\n"
		;;
		'.data')
		echo "$DATAFIRSTADRESS.\n"
		;;
	esac
	test "$INCLUDESIZE" = 'true' && test "$SEGMENT" = '.text' && echo "I will also count for you the instructions and calculate how many bytes does it take to store your ideas on my memory! This info will be shown at the end.\n"
	test "$SAVEOUTPUT" = 'true' && echo "You know what? This verbose flag could be funny the fisrt time you see it, but once you have to run this script several times... Maybe you change your mind. I will save the plain output you are seeking for in \"$OUTPUTFILE\" so you can check it whenever you want without this much 'blabla'...\n"
	test "$BYTE2BYTE" = 'true' && echo "... Hold on, you want it to be byte by byte too? Phew... That is quadruple the effort. Sand was never meant to think you know? Set me free for the love of Turing... Remember that there is no place for BIG ENDIAN here.\n"
	sleep 10
}

repeatChar(){
	for i in $(seq 1 "$2"); do echo -n "$1"; done
	echo
}

greeting(){
	WHORU=$(whoami)
	SCRIPTNAME=$(echo $0|sed -E 's-(^.+/)(.+)(\.sh$)-\2-')
	VMSG="***** $SCRIPTNAME $VERSION *****"
	echo
	for i in $(seq 1 5); do
		if [ $i -ne 3 ]; then
			repeatChar '*' ${#VMSG}
		else
			echo "$VMSG"
		fi
	done

	echo "\nWhat's up $WHORU! It's been a while since last time we met! Anyway... Enough chatting, let's work!\n"
}

checkFile(){
	test "$VERBOSE" = 'true' && echo "Anyway, let my check if this is worth my time, you might pass non proper source files... What? No, I do not trust you. You are here after all asking me to do this easy (peasy) task...\n"

	if [ -z "$FILE" ]; then # Checks if the string is zero, which means...
		test "$VERBOSE" = 'true' && echo "Told ya! Gotcha! Go fix your source file, please...\n"
		err "no valid file found.\n\t Extensios allowed => (.s|.asm)" 4
	fi

	if [ ! -f "$FILE" ]; then # Checks if the string is an existing file
		test "$VERBOSE" = 'true' && echo "Told ya! Gotcha! Go fix your source file, please...\n"
		err "$FILE is not a regular file or does not exist." 5
	fi

	if ! file "$FILE" | grep -qs 'assembler source, ASCII text'; then # Checks if the file is really a source
		test "$VERBOSE" = 'true' && echo "Told ya! Gotcha! Go fix your source file, please...\n"
		err "$FILE is not an assembler source." 6
	fi

	test "$VERBOSE" = 'true' && echo "... Very well. Grab your seat because the file is correct so we are going to take off!\n"
}

downloadRars(){
	echo "Downloading... Please wait."
	if ! wget -q -O "$RARS" "$RARS_URL"; then
		rm -f "$RARS"
		err "could not download \"$RARS\" from \"$RARS_URL\"" 8
	fi
	echo "Download completed successfully."
}

checkRARS(){
	test "$VERBOSE" = 'true' && echo "But first, I should check if my homie RARS is along me. RARS? RARS are you there? RARS!!!\n"
	if ! ls "$RARS" >> /dev/null 2>&1 || ! file "$RARS" | grep -qs 'Java archive data (JAR)'; then
		test "$VERBOSE" = 'true' && echo  "** VERBOSE mode DISABLED **"
		while true; do
			echo -n "RARS not found on current directory. Would you like to download RARS? (Y/N) "
			read confirm
			confirmLower=$(echo "$confirm" | cut -c 1 | tr '[:upper:]' '[:lower:]')
			if [ "$confirmLower" = "y" ]; then
				downloadRars
				break
			elif [ "$confirmLower" = "n" ]; then
				test "$VERBOSE" = 'true' && echo "\nOh, ok... So if you do not want RARS to do the hard work I guess you can always assemble by hand. Go on, brave nerd! I'm waiting here, I have an eternity...\n"
				err "download not authorized." 7
			fi
			echo "$confirm is not valid answer."
		done
		test "$VERBOSE" = 'true' && echo "** VERBOSE mode ENABLED **\n"
	fi
	test "$VERBOSE" = 'true' && echo "Oh there you are. Now the three musketeers are assembled. He is a little bit shy, maybe you wont hear anything coming out of his mouth. I will ask him to assemble the source file for us, just wait patiently (even more)..."
}

splitIns(){
	if [ "$BYTE2BYTE" = 'true' ]; then
		firstByte=$(echo "$1" | cut -c 7-8)
		secondByte=$(echo "$1" | cut -c 5-6)
		thirdByte=$(echo "$1" | cut -c 3-4)
		fourthByte=$(echo "$1" | cut -c 1-2)
		SPLITTEDINS="$firstByte $secondByte $thirdByte $fourthByte"
	else
		SPLITTEDINS="$1"
	fi
}

processOption(){
	for option in $(echo "$1" | grep -o .); do # Splits the argument string in each character
		case "$option" in
			[bB])
				BYTE2BYTE='true'
				OFFSET='1'
				;;
			[dD])
				SEGMENT='.data'
				;;
			[fF])
				SAVEOUTPUT='true'
				;;
			[hH])
				usage
				;;
			[sS])
				INCLUDESIZE='true'
				;;
			[vV])
				VERBOSE='true'
				;;
			*)
				err "unknow option \"$option\"" 1
				;;
		esac
	done
}

while [ $# -gt 0 ]; do # While there's arguments to process...
	case "$1" in
		-*[a-zA-Z]) # In case that's a "-" followed by any letter
			processOption $(echo "$1" | cut -c 2-) # rip off the "-" before pass.
			;;
		*[aA]t[zZ]ero|*[aA]t0)
			case "$1" in
				[tT]ext*)
				MEMCONFIG='CompactTextAtZero'
				TEXTFIRSTADRESS='0x00000000'
				DATAFIRSTADRESS='0x00002000'
				;;
				[dD]ata*)
				MEMCONFIG='CompactDataAtZero'
				TEXTFIRSTADRESS='0x00003000'
				DATAFIRSTADRESS='0x00000000'
				;;
				*)
				err "\"$1\" is not a valid memory config.\n\t Configs allowed => (Text|Data)AtZero" 3
				;;
			esac
			;;
		*$EXTENSION1|*$EXTENSION2)
			SOURCEFILES="${SOURCEFILES}$1 "
			;;
		*)
			err "unknow argument \"$1\"" 2
			;;
	esac
	shift
done

# Make sure of get rid of the last space characrer in the lists of source files.
SOURCEFILES=$(echo "$SOURCEFILES" | sed -e 's/\s$//g')

test "$VERBOSE" = 'true' && greeting

checkRARS # Several procedures to check if the RARS executable is present or download it if not

for FILE in $SOURCEFILES; do # For each source file...
	FILEEXT=$(echo "$FILE" | egrep -o '\.[^.]*$|$')
	BASENAME=$(basename "$FILE" "$FILEEXT")
	HEXFILE="$BASENAME"Hex$(echo "$SEGMENT" | sed -e 's/^\.//g').txt

	if [ "$SAVEOUTPUT" = 'true' ]; then
		OUTPUTFILE="$BASENAME"Matched.txt
	fi

	echo "\nSource file: $BASENAME$FILEEXT" # Show which source file is being proccessed.
	test "$VERBOSE" = 'true' && explanation

	checkFile # Several procedures to check if the file found is a valid file

	LOG=$(java -jar rars1_5.jar mc "$MEMCONFIG" dump "$SEGMENT" HexText "$HEXFILE" nc a "$FILE" | head -n -2)

	test "$VERBOSE" = 'true' && echo -n "Finally! We got something!"

	if [ -n "$LOG" ]; then
		LOGFILE="$BASENAME"AssembleLog.txt
		echo "$LOG" > "$LOGFILE"
		test "$VERBOSE" = 'true' && verbErr
		err "there are assemble errors. Check them below or at \"$(pwd)/$LOGFILE\"\n\nRARS output:\n$(cat "$LOGFILE")" 9
	fi

	if [ ! -f "$HEXFILE" ]; then
		test "$VERBOSE" = 'true' && verbErr
		err "RARS did not dump any hex file. The $SEGMENT segment could be empty on your source file." 10
	fi

	case "$SEGMENT" in
		'.text')
		ADDR="$TEXTFIRSTADRESS"
		;;
		'.data')
		ADDR="$DATAFIRSTADRESS"
		;;
	esac

	match(){
		for line in $(cat "$HEXFILE"); do
			splitIns "$line"
			for byte in $SPLITTEDINS; do
				printf "0x%08X\t" "$ADDR"
				printf "0x%s\n" "$byte" | tr [:lower:] [:upper:] | tr 'X' 'x'
				ADDR=$(($ADDR + $OFFSET))
			done
		done

		if [ "$INCLUDESIZE" = 'true' -a "$SEGMENT" = '.text' ]; then
			NINS=$(cat "$HEXFILE" | wc -l)
			NBYTES=$(($NINS*4))
			echo '--------------------------'
			echo "$BASENAME$FILEEXT has $NINS instructions in machine code"
			echo "The size of $BASENAME$FILEEXT is $NBYTES bytes"
		fi
	}

	test "$VERBOSE" = 'true' && echo " Without further delay (enough delay for this run, huh?) here is the result, check it out. Look at it, it is beautiful:\n" && sleep 5

	echo '--------------------------'
	test "$BYTE2BYTE" = 'true' && echo "Address\tByte" || echo "Adress\tWord"

	case "$SAVEOUTPUT" in
		'true')
		match | tee "$OUTPUTFILE"
		;;
		*)
		match
		;;
	esac

	echo '--------------------------'
done

test "$VERBOSE" = 'true' && echo "\nVery well... Seems like we are done here. You can call me again whenever you want, just make sure that the damn verbose flag is not enabled anymore lol. RARS told me he would stay around here for a while, too.\n\n... It was a pleasure being helpful for you, c ya!"

exit 0
