# **hexMatcher**

**Extract info** related with the _memory tree_ from one or more assembler source files from **RARS**.

## Description

To fully understand the behavior of a low level program it is helpful to visualize how the memory is managed, among other things. As an example:

1.  Machine code and where is stored.
	- Either with aligned _words_ or...
	- Byte by byte.
2.  Assembler source size.
	- Either with aligned _words_ or...
	- Byte by byte.
3.  ... And so much more!

However this could became tedious if you have to analyze multiple programs (With its multiple instructions...)
So this script will provide a quick workflow to extract that sort of info on demand, from the source code and nothing more.
You may also want to keep that info stored in a file for future reference. This script will do the job.

> RARS 1.5 will be included in the repo just in case you could not download it, however by the moment is **mandatory** that the RARS executable is located at the same directory as the script. (If not, the script will ask you to download)

## Usage

```sh
hexMatcher.sh -[OPTION...] [TextAtZero|DataAtZero] FILE.s|FILE.asm...
```

- [ ] FILE.s|FILE.asm
  * The source file. Both extensions are allowed. It is the only **mandatory** argument. At least one is needed, but you can pass as many as you need. Just make sure that you space them.
- [ ] TextAtZero|DataAtZero
  * The memory configuration. This will change the first address of each segment. By default, these are:
    * _.text_ at **0x00400000**.
    * _.data_ at **0x10010000**.
  * However, with each self-explanatory option, they will change to:
    * ___TextAtZero___
      * _.text_ at **0x0000000**.
      * _.data_ at **0x00002000**.
    * ___DataAtZero___
      * _.text_ at **0x00003000**.
      * _.data_ at **0x00000000**.
- [ ] -b, -B
  * Byte. The output will be formatted as one byte per address. By default the output is formatted as one word per aligned address.
- [ ] -d, -D
  * Data. The default segment to dump will be the _.text_ segment. With this option, the dumped segment will be the _.data_ segment. **This will disable the _-sS_ flag.**
- [ ] -f, -F
  * File. Will create a plain text file with the output in the same directory where the script is executed.
- [ ] -h, -H
  * Help. Print the usage of the script.
- [ ] -s, -S
  * Size. Will print in its standard output the total size of the source file, either in words (Instructions...) and bytes. This info its included on the file created if _-fF_ is enabled, in the last two rows of it. **If the segment dumped is '.data', no info will be displayed since RARS by default dumps the first 1024 addresses of said segment. (That means 4096 bytes though)**
- [ ] -v, -V
  * Verbose. Will print in its standard output info about the current process that is running. Might not be that useful, but might be a bit funny. **This messages are _NOT_ included in the file made by _-fF_ if both are enabled in the same run.**

You can either write the options one by one or all together (Or a mix...), up to you!

In order to work, this script needs to assemble the code with the help of RARS.
However this assemble might go wrong, in that case the script will stop just after print the assemble errors prompted by RARS. It will also create a log file in the same directory where the script was executed for future reference.

## Visuals

```sh
src/hexMatcher.sh resources/contador1.asm
```
![Simple run](./pictures/simpleDownloadingRars.png)

```sh
src/hexMatcher.sh -b resources/contador1.asm
```
![Byte by byte flag enabled](./pictures/simpleByte.png)

```sh
src/hexMatcher.sh -s resources/contador1.asm
```
![Size flag enabled](./pictures/simpleSize.png)

```sh
src/hexMatcher.sh -f resources/contador1.asm
```
![Output file flag enabled](./pictures/simpleFileOutput.png)

```sh
src/hexMatcher.sh TextAtZero|DataAtZero resources/contador1.asm
```
![Segments at zero](./pictures/simpleSegmentAtZero.png)

```sh
src/hexMatcher.sh resources/contador?.asm
```
![Multiple source files](./pictures/simpleMultiple.png)

```sh
src/hexMatcher.sh resources/errExamples/contador1err.asm
```
![Segments at zero](./pictures/simpleAssembleErrors.png)

## Support

Normally, you could get in touch with the OG programmer of the repo for any reason you may encounter while using it. But you know what, I'm so confident on you that I think that you could resolve any problem on your own. Clone it, modify it, make it yours, this is your new life! However I'm not that evil so, here is two alternatives if you have nothing else to try:

- [ ] [I'm partially lost](https://www.youtube.com/watch?v=dQw4w9WgXcQ)
- [ ] [I'm fully lost](https://github.com/Obijuan) I owe my life to this guy. He will save you. He has nothing to do with this repo but he's probably your teacher so squeeze him like you paid for your studies. <sub>_Hold on actually I've done that_</sub>

However as a rule of thumb, my grandfather used to say:
> Búscate la vida, gandul.

He made me learn [the hard way](https://www.youtube.com/watch?v=3ILscTrJrY4) though.

## Installation

This script should work in the [linux labs](https://labs.etsit.urjc.es/) (_Ubuntu 20.04 Focal Fossa_) available on the URJC without installing anything, however is ___mandatory___ to have an executable [RARS 1.5](https://github.com/TheThirdOne/rars/releases/download/v1.5/rars1_5.jar) in the same directory where the script is executed.

Don't worry though! If you don't have it the script will warn you and ask if you want to automatically download it, on the go!

## Exit errors

Although the script will always exist with an error message, here you can find a more detailed explanation of every error contemplated. The script will exit with that exact number too.

1. The option passed is not known.
2. The argument passed to the script does not match anything meaningful.
3. Non valid memory config option. (FridgeAtZero is not a valid memory config)
4. Wrong extension or no file passed. Its mandatory to have one file with _.s_ or _.asm_ extension.
5. File passed does not exist.
6. File passed exists, but is not a source file.
7. Download of RARS was not authorized by the user.
8. Download of RARS was not possible (Network failed, server down...)
9. RARS could not assemble the source file because there are assemble errors.
10. RARS could assemble the source file, but did not dump any hex file. (The segment could be empty)


## Authors and acknowledgment

Although I am the only author for this humble piece of code, none a single letter could be written without their help.

- [ ] [He taught me RARS and RISC-V](https://twitter.com/Obijuan_cube)
- [ ] [He taught me git and shell scripting](https://twitter.com/paurea)
- [ ] [He is the reason why I can publish this repo for you](https://twitter.com/AgutierrURJC)

## License

Currently considering [MIT](https://choosealicense.com/licenses/mit/) license.

## Project status

Working according to the Roadmap.

## Roadmap

- [x] In case of a failed assemble, log the errors in a file.
- [x] Work with multiple source files in one run.
**UPDATE**
The way this feature was implemented was by running the entire script iterating by all of the source files passed.
That means that whenever an error is detected in any file, the script will exit **WITHOUT** proccesing the remaining files.
***The same assemble configuration will be applied to every source file passed***
<sub>_Disclaimer_: This also means that the verbose flags are applied in every iterarion... Last warning.</sub>
- [ ] Search for the RARS simulator instead of download it each time. (If not found on the same directory where the script is executed)
- [ ] Compact "last zeros" when .data segment is dumped and analyzed.

### More useful info

Go check the labs [tutorials](https://labs.etsit.urjc.es/index.php/tutoriales/), you might find something useful. (Tired of typing your password every time you want to start an SSH session, maybe...?) as well as the [etsit repo.](https://labs.etsit.urjc.es/repo/).

#### Obijuan approval

- [x] Opt. to show the size by bytes and words.
- [x] Opt. to download RARS. * **Improved** * No option needed, the script will pause and try to download it on the go, then continue with the normal behavior. Only stops when the download is not authorized or not possible.
- [x] Opt. to work with source files. * **Improved** * No option needed, the script will detect if the file passed is a source file and will no longer work with plain text hexadecimal files. This decision was made due to lots of information are lost with plain text format. (Like the memory configuration, the segment that was dumped...)
- [x] Show the assemble errors if any. * **Improved** * This errors will be stored in a log file automatically.
